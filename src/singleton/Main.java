/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author Gustavo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Singleton arsenal = Singleton.getInstance("Arsenal");
       System.out.println(arsenal.getNombre());
       
       Singleton chelsea = Singleton.getInstance("Chelsea");
       System.out.println(chelsea.getNombre());
    }    
}
