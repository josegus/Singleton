package singleton;

public class Singleton {
    
    private static Singleton singleton;
    private String nombre;
    
    // El constructor es privado. No permite que se genere un constructor
    private Singleton(String nombre) {        
        this.nombre = nombre;
    }
    
    public static Singleton getInstance(String nombre) {
        if (singleton == null) {
            singleton = new Singleton(nombre);
        } else {
            System.out.println("No se puede crear el objeto "+ nombre + " porque ya existe un objeto de la misma clase. Retornando objeto ya creado..");
        }
        return singleton;
    }
    
    public String getNombre() {
        return nombre;
    }
}
